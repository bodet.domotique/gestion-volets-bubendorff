# Gestion volets bubendorff

Gérer l'ouverture et la fermeture avec un wemos D1 mini (ESP8266).

J'ai intégré la librairie WifiManager afin de gérer les accès wifi.
Le code créé un serveur web à l'écoute de requête http "OUVRIR" ou "FERMER"