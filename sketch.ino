#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

int tempo = 250;
String s;
int val;
int up = 4;    // Wemos D2
int down = 5;  // Wemos D1


// Creation d un serveur qui ecoute sur le port 80
WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  delay(10);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
    
  //reset settings
  // wifiManager.resetSettings();
    
  //set custom ip for portal
  wifiManager.setAPStaticIPConfig(IPAddress(10,0,0,200), IPAddress(10,0,0,200), IPAddress(255,255,255,0));

  //fetches ssid and pass from eeprom and tries to connect
  //if it does not connect it starts an access point with the specified name
  //and goes into a blocking loop awaiting configuration
  wifiManager.autoConnect("ESP_Connect");

  //if you get here you have connected to the WiFi
  Serial.println("ESP : connecté");

  // Initialisation de l'OTA
  ArduinoOTA.setHostname("monEsp");
  ArduinoOTA.begin(); 
  
  // Declaration port "down" en sortie
  pinMode(down, OUTPUT);
  digitalWrite(down, LOW);

  // Declaration port "up" en sortie
  pinMode(up, OUTPUT);
  digitalWrite(up, LOW);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  // demarrage du serveur
  server.begin();
  Serial.println("ESP : Server HTTP OK");

  // Adresse IP locale attribuee
  Serial.print("ESP : IP : ");
  Serial.println(WiFi.localIP());
}


void loop() {
   
   // Vérification si une MAJ est en attente
   ArduinoOTA.handle(); 
  
  // Un client est-il connecte ?
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  
  client.print(pageHTML());

  // Attente donnees envoyees par un client
  Serial.println("Nouveau client");
  while (!client.available()) {
    delay(1);
  }

  // Lecture premiere ligne de la requete
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();

  if (req.indexOf("/LED") != -1)
  {
    digitalWrite(LED_BUILTIN, LOW); 
  }

  else if (req.indexOf("/OFF") != -1)
  {
    digitalWrite(LED_BUILTIN, HIGH); 
  }

  //------------------------------- FERMER -------------------------------------------------
 else if (req.indexOf("/FERMER") != -1) 
  {
    Serial.println("Demande fermeture volet");

    // Positionnement port "down"
    digitalWrite(down, 1);                   // Début appui pour fermeture volet
    delay(tempo);
    digitalWrite(down, 0);                    // Fin appui pour fermeture volet


    client.print(reponseHTML());
    // give the web browser time to receive the data
    delay(1);
    
    blink();
    
    //client.stop();
    //return;
    Serial.println("client déconnecté");
  }

  //------------------------------- OUVRIR -------------------------------------------------
  else if (req.indexOf("/OUVRIR") != -1)
  {
    Serial.println("Demande ouverture volet");
    
    // Positionnement port "up"
    digitalWrite(up, 1);                   // Début appui pour ouverture volet
    delay(tempo);                           //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    digitalWrite(up, 0);                    // Fin appui pour ouverture volet


    client.print(reponseHTML());
    // give the web browser time to receive the data
    delay(1);

    blink();
    
    //client.flush();
    //client.stop();
    //return;
    Serial.println("client déconnecté");
  }
  else if (req.indexOf("/FERMETURE_PARTIELLE") != -1) 
  {
    Serial.println("Demande fermeture partielle volet");

    // Positionnement port "down"
    digitalWrite(down, 1);                   // Début appui pour fermeture volet
    delay(tempo);
    digitalWrite(down, 0);                    // Fin appui pour fermeture volet

delay(12000)

digitalWrite(down, 1);                   
    delay(tempo);
    digitalWrite(down, 0); 
    
    client.print(reponseHTML());
    // give the web browser time to receive the data
    delay(1);
    
    blink();
    
    //client.stop();
    //return;
    Serial.println("client déconnecté");
  }
  else if (req.indexOf("/OUVERTURE_PARTIELLE") != -1) 
  {
    Serial.println("Demande ouverture partielle volet");

    // Positionnement port "down"
    digitalWrite(up, 1);                   // Début appui pour fermeture volet
    delay(tempo);
    digitalWrite(up, 0);                    // Fin appui pour fermeture volet

delay(12000)

digitalWrite(up, 1);                 
    delay(tempo);
    digitalWrite(up, 0); 
    
    client.print(reponseHTML());
    // give the web browser time to receive the data
    delay(1);
    
    blink();
    
    //client.stop();
    //return;
    Serial.println("client déconnecté");
  }
 
}

String reponseHTML()
{
  String valeur="ok";
  
  return valeur;
}

String pageHTML()
{
  String html="HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\n";
  html+="<br><input type=\"button\" name=\"b1\" value=\"Ouvrir\" onclick=\"location.href='/OUVRIR'\">";
  html+="<br><br>";
  html+="<br><input type=\"button\" name=\"b1\" value=\"Fermer\" onclick=\"location.href='/FERMER'\">";
  html+="</html>\n";
  html+="<br><br>";
  
  return html;
}

void blink()
{
    digitalWrite(LED_BUILTIN, LOW);
    delay(500);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(500);
    digitalWrite(LED_BUILTIN, LOW);
    delay(500);
    digitalWrite(LED_BUILTIN, HIGH);
}